# 環境構築

DockerでのRailsローカル開発環境です。  
こちらを参考にしました  
https://qiita.com/kodai_0122/items/795438d738386c2c1966


## 初回環境構築手順
dockerビルド

```
$ docker-compose build
```

プラグインのインストール  
```
$ docker-compose run web yarn
```

webpackerのインストール
```
$ docker-compose run web bundle exec rails webpacker:install
```

コンテナ起動とDBの作成
```
$ docker-compose up
$ docker-compose run web rake db:create
```

Railsが起動していることを確認  
http://localhost:3000

# Herokuデプロイ

こちらを参考に  
https://qiita.com/fuku_tech/items/dc6b568f7f34df10cae7  

```
# herokuのコンテナレジストリにログイン
heroku container:login

# 新しいappを作成
heroku create

# イメージを作成してコンテナレジストリにpush
heroku container:push web

# postgresqlアドオンの無料プランを追加
heroku addons:create heroku-postgresql:hobby-dev

# DBセットアップ
heroku run rails db:migrate

# イメージをherokuへデプロイ
heroku container:release web

# 実際にアクセスして/usersを確認してみる
heroku open
```